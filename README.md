# A simple Flask application with form functionality

## Requirements
- Python 3.0 and above
- Flask library

## Functionality

- This is application will be able to get data from a form.
- Store it in a temporary .txt file.
- Displays the entered form data.

## Purpose
- This project has been made as a part of submission for the assessment task 1 in DevOps from WGS.
- A CI/CD pipeline is being setup as the objective of this assessment states the same.

## Deployment
- There were few mistakes, while bringing the code up to production level.
    - port number was changed.
    - changing variables for ci configurations to get the .txt file.
    
